<?php

	class Product
	{
		public $name;
		public $nameFontSize;
		public $price;
		public $priceFontSize;
		public $weight;
		public $weightFontSize;
		public $image;
		public $border;
		public $bg;

		public function __construct(string $name, int $price, float $weight, $image, string $border, string $bg, int $nameFontSize = 20, int $priceFontSize = 16, int $weightFontSize = 16)
		{
			$this->name = $name;
			$this->nameFontSize = $nameFontSize;
			$this->price = $price;
			$this->priceFontSize = $priceFontSize;
			$this->weight = $weight;
			$this->weightFontSize = $weightFontSize;
			$this->image = $image;
			$this->border = $border;
			$this->bg = $bg;
		}

		public function printProduct()
		{
			echo "<div>
			<h2 style='font-size: {$this->nameFontSize}px'> {$this->name} </h2>
			<span>	Цена: {$this->price} руб. <br> Вес: {$this->weight} кг. </span>
			</div>";
		}

		public function showImage()
		{
			echo "<div> <img src='{$this->image}' alt='Картинка' width='200'>
			</div>";
		}
	}

	class PrintPrice extends Product
	{
		public $priceNotNds;
		public $priceNotNdsFontSize;

		public function __construct(string $name, int $price, int $priceNotNds, float $weight, $image, string $border, string $bg, int $nameFontSize = 20, int $priceFontSize = 16, int $priceNotNdsFontSize = 16, int $weightFontSize = 16)
		{
			$this->priceNotNds = $priceNotNds;
			$this->priceNotNdsFontSize = $priceNotNdsFontSize;
		
			parent::__construct($name, $price, $weight, $image, $border, $bg, $nameFontSize, $priceFontSize, $weightFontSize);
		}

		public function printProduct()
		{
			echo "<div style='border: {$this->border}; background: {$this->bg};'>
			<h2 style='font-size: {$this->nameFontSize}px'>{$this->name} </h2>
			<span> Стоимость товара: {$this->price} руб. <br> Стоимость товара без учёта НДС: {$this->priceNotNds} руб. </span>
			</div>";
		}

	}

	class Chocolate extends Product
	{
		public $kkal;

		public function __construct(string $name, int $price, float $weight, float $kkal, $image, string $border, string $bg, int $nameFontSize = 20, int $priceFontSize = 16, int $weightFontSize = 16)
		{
			$this->kkal = $kkal;
		
			parent::__construct($name, $price, $weight, $image, $border, $bg, $nameFontSize, $priceFontSize, $weightFontSize);
		}

		public function printInfProduct()
		{
			echo "<div style='border: {$this->border}; background: {$this->bg};'>
			<h2 style='font-size: {$this->nameFontSize}px'>{$this->name} </h2>
			<span>	Цена: {$this->price} руб. <br> Вес: {$this->weight} кг. <br> Ккал: {$this->kkal} </span>
			</div>";
		}
	}

	class Candy extends Product
	{
		public $kkal;

		public function __construct(string $name, int $price, float $weight, float $kkal, $image, string $border, string $bg, int $nameFontSize = 20, int $priceFontSize = 16, int $weightFontSize = 16)
		{
			$this->kkal = $kkal;
		
			parent::__construct($name, $price, $weight, $image, $border, $bg, $nameFontSize, $priceFontSize, $weightFontSize);
		}

		public function printInfProduct()
		{
			echo "<div style='border: {$this->border}; background: {$this->bg};'>
			<h2 style='font-size: {$this->nameFontSize}px'>{$this->name} </h2>
			<span>	Цена: {$this->price} руб. <br> Вес: {$this->weight} кг. <br> Ккал: {$this->kkal} </span>
			</div>";
		}

		public function showImage()
		{
			echo "<div> <img src='{$this->image}' alt='Картинка' width='100'>
			</div>";
		}
	}


	echo "<div style= background: aqua;'><h2><center> Домашнее задание №8 </center></h2></div>";
	echo '<br>';
	$showImageNews = new Chocolate('Twix', '50', '42', '0.100', 'Chocolate.jpg', '1px solid black', 'aqua');
	$showImageNews -> showImage();
	$productsNews = new Product('Twix', '50', '0.100', 'Chocolate.jpg', '1px solid black', 'aqua');
	$productsNews -> printProduct();
	echo '<br>';
	$printInfoProductNews = new Chocolate('Заказ 1', '50', '0.100', '150', 'Chocolate.jpg', '1px solid black', 'aqua');
	$printInfoProductNews -> printInfProduct();
	echo '<br>';
	echo '<br>';
	$showImageNews1 = new Candy('Raffaello', '200', '180', '0.300', 'Raffaello.jpg', '1px solid black', 'aqua');
	$showImageNews1 -> showImage();
	$productsNews1 = new Product('Raffaello', '200', '0.300', 'Raffaello.jpg', '1px solid black', 'aqua');
	$productsNews1 -> printProduct();
	echo '<br>';
	$printPriceNews1 = new Candy('Заказ 1', '200', '0.300', '550', 'Raffaello.jpg', '1px solid black', 'aqua');
	$printPriceNews1 -> printInfProduct();

?>